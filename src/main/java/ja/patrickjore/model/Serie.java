package ja.patrickjore.model;

import java.util.ArrayList;

public class Serie {
    private Integer id;
    private String name;
    private String director;
    private String description;

    public Serie(Integer id, String name, String director, String description) {
        this.id = id;
        this.name = name;
        this.director = director;
        this.description = description;
    }

    public Serie() {}

    // Return list of all information about this serie
    public ArrayList<String> getAll() {
        ArrayList<String> info = new ArrayList<String>();
        info.add(this.id.toString());
        info.add(this.name);
        info.add(this.director);
        info.add(this.description);
        return info;
    }

    // Get functions
    public Integer getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getDirector() {
        return this.director;
    }

    public String getDescription() {
        return this.description;
    }

    //Set functions
    public void setName(String name) {
        this.name = name;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}