package ja.patrickjore.controller;

import ja.patrickjore.service.*;
import ja.patrickjore.model.*;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.awt.*;

@RestController
@RequestMapping("/series")
public class SerieController {

    private SerieCollectionService service;

    public SerieController(SerieCollectionService service) {
        this.service = service;
    }

    @RequestMapping(value = "", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Serie> getSeries() {
        return service.getSeries();
    }

    @RequestMapping(value = "{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Serie getSerie(@PathVariable String id) {
        return service.getSerie(Integer.parseInt(id));
    }

    @RequestMapping(value = "", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public void addSerie(@RequestBody Serie serie) {
        service.addSerie(serie);
    }

}