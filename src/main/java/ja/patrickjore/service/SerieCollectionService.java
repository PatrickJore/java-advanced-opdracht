package ja.patrickjore.service;

import ja.patrickjore.model.*;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class SerieCollectionService {

    private List<Serie> series = new ArrayList<>(
            Arrays.asList(
                    new Serie(1,"Chernobyl", "Craig Mazin", "In April 1986, an explosion at the Chernobyl nuclear power plant in the Union of Soviet Socialist Republics becomes one of the world's worst man-made catastrophes."),
                    new Serie(2, "Planet Earth II", "David Attenborough", "David Attenborough returns in this breathtaking documentary showcasing life on Planet Earth."),
                    new Serie(3, "Breaking Bad", "Vince Gilligan", "A high school chemistry teacher diagnosed with inoperable lung cancer turns to manufacturing and selling methamphetamine in order to secure his family's future."),
                    new Serie(4, "Game of Thrones", "David Benioff, D.B. Weiss", "Nine noble families fight for control over the mythical lands of Westeros, while an ancient enemy returns after being dormant for thousands of years."),
                    new Serie(5, "Lost", "J.J. Abrams, Jeffrey Lieber, Damon Lindelof", "The survivors of a plane crash are forced to work together in order to survive on a seemingly deserted tropical island.")
            )
    );

    public List<Serie> getSeries() {
        return series;
    }

    public Serie getSerie(Integer id) {
        for(Serie serie : series) {
            if(serie.getId().equals(id)) {
                return serie;
            }
        }

        return null;
    }

    public void addSerie(Serie serie) {
        series.add(serie);
    }
}